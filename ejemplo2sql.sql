﻿/* codigo para generar */

-- crea base de datos
DROP DATABASE IF EXISTS b20190529;
CREATE DATABASE b20190529;
USE b20190529;

-- creando tablas
-- clientes
CREATE TABLE cliente(
  -- campos
  dni varchar(10),
  nombre varchar(25),
  apellidos varchar(100),
  tfno varchar(15),
  fecha_nac date,
  -- claves
  PRIMARY KEY(dni)
);

-- productos
CREATE TABLE producto(
  -- campos
  cod int AUTO_INCREMENT,
  nombre varchar(25),
  precio float,
  -- claves
  PRIMARY KEY(cod)
);

-- proveedores
CREATE TABLE proveedor(
  -- campos
  nif varchar(12),
  nombre varchar(50),
  direccion varchar(50),
  -- claves
  PRIMARY KEY(nif)
);

-- compra
CREATE TABLE compra(
  -- campos
  dniCliente varchar(10),
  codProducto int,
  -- claves
  PRIMARY KEY(dniCliente,codProducto),
  
  -- claves ajenas
  CONSTRAINT FKcompracliente FOREIGN KEY(dniCliente)
  REFERENCES cliente(dni),

  CONSTRAINT FKcompraproducto FOREIGN KEY(codProducto)
  REFERENCES producto(cod)
);

-- suministra
CREATE TABLE suministra(
  -- campos
  codProducto int,
  nifProveedor varchar(12),
  -- claves
  PRIMARY KEY(codProducto,nifProveedor),

  UNIQUE KEY(codProducto),

  -- claves ajenas
  CONSTRAINT FKsuministraproducto FOREIGN KEY(codProducto)
  REFERENCES producto(cod),

  CONSTRAINT FKsuministraproveedor FOREIGN KEY(nifProveedor)
  REFERENCES proveedor(nif)
);
